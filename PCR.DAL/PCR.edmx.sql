
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/11/2018 11:32:32
-- Generated from EDMX file: C:\Users\gvera\Documents\Visual Studio 2015\Projects\PCR.DAL\PCR.DAL\PCR.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [PCR];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[PROJECT]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PROJECT];
GO
IF OBJECT_ID(N'[dbo].[SUBJOB]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SUBJOB];
GO
IF OBJECT_ID(N'[dbo].[DIVISION]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DIVISION];
GO
IF OBJECT_ID(N'[dbo].[EMPLOYEE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EMPLOYEE];
GO
IF OBJECT_ID(N'[dbo].[ENR_MARKET]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ENR_MARKET];
GO
IF OBJECT_ID(N'[dbo].[NAICS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NAICS];
GO
IF OBJECT_ID(N'[dbo].[PROJECT_NAICS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PROJECT_NAICS];
GO
IF OBJECT_ID(N'[dbo].[SCOPE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SCOPE];
GO
IF OBJECT_ID(N'[dbo].[PROJECT_SCOPE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PROJECT_SCOPE];
GO
IF OBJECT_ID(N'[dbo].[DISCIPLINE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DISCIPLINE];
GO
IF OBJECT_ID(N'[dbo].[PROJECT_DISCIPLINE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PROJECT_DISCIPLINE];
GO
IF OBJECT_ID(N'[dbo].[CONTRACT_TYPE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CONTRACT_TYPE];
GO
IF OBJECT_ID(N'[dbo].[GREEN_CERTS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GREEN_CERTS];
GO
IF OBJECT_ID(N'[dbo].[CRM_CUSTOMER]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CRM_CUSTOMER];
GO
IF OBJECT_ID(N'[dbo].[CRM_SYSTEM]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CRM_SYSTEM];
GO
IF OBJECT_ID(N'[dbo].[SUBJOB_NAICS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SUBJOB_NAICS];
GO
IF OBJECT_ID(N'[dbo].[SUBJOB_SCOPE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SUBJOB_SCOPE];
GO
IF OBJECT_ID(N'[dbo].[SUBJOB_DISCIPLINE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SUBJOB_DISCIPLINE];
GO
IF OBJECT_ID(N'[dbo].[JOINT_VENTURE_PARTNER]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JOINT_VENTURE_PARTNER];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'PROJECT'
CREATE TABLE [dbo].[PROJECT] (
    [PCR_ID] bigint IDENTITY(1,1) NOT NULL,
    [CRM_RECORD_NO] nvarchar(50)  NULL,
    [CRM_SYSTEM_ID] bigint  NULL,
    [ERP_MASTER_PROJECT_NO] nvarchar(12)  NOT NULL,
    [PROJECT_STAGE] nvarchar(12)  NOT NULL,
    [PURSUIT_LEADER] nvarchar(200)  NOT NULL,
    [PROJECT_DIRECTOR] nvarchar(200)  NOT NULL,
    [IS_CUSTOMER_NEW] bit  NOT NULL,
    [CUSTOMER_ID] bigint  NOT NULL,
    [CONTACT_FIRST_NAME] nvarchar(150)  NOT NULL,
    [CONTACT_LAST_NAME] nvarchar(150)  NOT NULL,
    [CONTACT_PHONE] nvarchar(50)  NOT NULL,
    [CONTACT_EMAIL] nvarchar(150)  NOT NULL,
    [OFFICIAL_TITLE] nvarchar(30)  NOT NULL,
    [PROJECT_CODE] nvarchar(20)  NOT NULL,
    [COUNTRY] nvarchar(150)  NOT NULL,
    [ADDRESS] nvarchar(500)  NOT NULL,
    [CITY] nvarchar(200)  NOT NULL,
    [REGION_STATE] nvarchar(200)  NOT NULL,
    [IS_PROJECT_DATA_RESTRICTED] bit  NOT NULL,
    [DESCRIPTION] nvarchar(40)  NOT NULL,
    [SIZE_CHARACTERISTICS] nvarchar(500)  NOT NULL,
    [SITE_CHARACTERISTICS] nvarchar(500)  NOT NULL,
    [CUSTOMER_PROPOSAL_NO] nvarchar(50)  NOT NULL,
    [PROPOSAL_DATE] datetime  NOT NULL,
    [IS_CONTRACT_EXECUTED_Y_N] nvarchar(3)  NOT NULL,
    [IS_NOTICE_TO_PROCEED_IN_PLACE] nvarchar(3)  NOT NULL,
    [CONTRACT_EXECUTED_EXPLAIN] nvarchar(500)  NOT NULL,
    [IS_GONOGO_ANALYISIS_COMPLETE] nvarchar(3)  NOT NULL,
    [IS_AE_RESEARCH_COMPLETE] nvarchar(3)  NOT NULL,
    [IS_CONST_LICENSE_COMPLETE] nvarchar(3)  NOT NULL,
    [IS_BUSINESS_LICENSE_COMPLETE] nvarchar(3)  NOT NULL,
    [IS_ANALYSIS_DOC_AVAILABLE] nvarchar(3)  NOT NULL,
    [GONOGO_ANALYSIS_DOC_PATH] nvarchar(500)  NOT NULL,
    [IS_REVENUE_ANTICIPATED] nvarchar(3)  NOT NULL,
    [AE_LABOR_AMT] decimal(20,2)  NOT NULL,
    [CONSULTANTS_AMT] decimal(20,2)  NOT NULL,
    [TRAVEL_AMT] decimal(20,2)  NOT NULL,
    [OTHER_AMT] decimal(20,2)  NOT NULL,
    [ESTIMATED_BILLING_AMT] decimal(20,2)  NOT NULL,
    [TOTAL_EXPENSE] decimal(20,2)  NOT NULL,
    [NET_EXPENSE] decimal(20,2)  NOT NULL,
    [IS_GREEN_PROJECT] bit  NOT NULL,
    [GREEN_PROJECT_CERT] nvarchar(100)  NOT NULL,
    [GREEN_PROJECT_CERT_OTHER] nvarchar(max)  NOT NULL,
    [IS_GP_PROSPECT_APPROVAL_NEEDED] bit  NOT NULL,
    [IS_CFO_PROJECT_APPROVAL_NEEDED] bit  NOT NULL,
    [DL_PROSPECT_APPROVAL_SIGN] nvarchar(100)  NOT NULL,
    [DL_PROSPECT_APPROVAL_ID] nvarchar(25)  NOT NULL,
    [DL_PROJECT_APPROVAL_SIGN] nvarchar(100)  NOT NULL,
    [DL_PROJECT_APPROVAL_ID] nvarchar(25)  NOT NULL,
    [DL_PROSPECT_APPROVAL_DATE] datetime  NOT NULL,
    [DL_PROJECT_APPROVAL_DATE] datetime  NOT NULL,
    [GP_PROSPECT_APPROVAL_SIGN] nvarchar(100)  NOT NULL,
    [GP_PROSPECT_APPROVAL_ID] nvarchar(25)  NOT NULL,
    [GP_PROJECT_APPROVAL_SIGN] nvarchar(100)  NOT NULL,
    [GP_PROJECT_APPROVAL_ID] nvarchar(25)  NOT NULL,
    [GP_PROSPECT_APPROVAL_DATE] datetime  NOT NULL,
    [GP_PROJECT_APPROVAL_DATE] datetime  NOT NULL,
    [CFO_PROJECT_APPROVAL_STAMP] nvarchar(max)  NOT NULL,
    [PROSPECT_APPROVAL_STATUS] nvarchar(100)  NOT NULL,
    [PROJECT_APPROVAL_STATUS] nvarchar(100)  NOT NULL,
    [PROSPECT_APPROVAL_COMMENTS] nvarchar(500)  NOT NULL,
    [PROJECT_APPROVAL_COMMENTS] nvarchar(500)  NOT NULL,
    [PURCHASE_ORDER_NO] nvarchar(25)  NOT NULL,
    [PURCHASE_ORDER_AMT] decimal(20,2)  NOT NULL,
    [PAYMENT_TERMS] nvarchar(200)  NOT NULL,
    [PROJECT_DEV_AMT] decimal(20,2)  NOT NULL,
    [IS_MSA_IN_PLACE] nvarchar(3)  NOT NULL,
    [CURRENT_CONTRACT_AMT] decimal(20,2)  NOT NULL,
    [ESTIMATED_JOB_PROFIT] decimal(20,2)  NOT NULL,
    [DESIGN_START_DATE] datetime  NOT NULL,
    [DESIGN_END_DATE] datetime  NOT NULL,
    [CONST_START_DATE] datetime  NOT NULL,
    [CONST_END_DATE] datetime  NOT NULL,
    [DELIVERY_METHOD] nvarchar(50)  NOT NULL,
    [DELIVERY_METHOD_OTHER] nvarchar(500)  NOT NULL,
    [SERVICE_TYPE] nvarchar(150)  NOT NULL,
    [CONTRACT_TYPE] nvarchar(150)  NOT NULL,
    [TYPE_OF_CONTRACT_DOCUMENT] nvarchar(50)  NOT NULL,
    [IS_JOINT_VENTURE] bit  NOT NULL,
    [IS_WBE_MBE_REQUIREMENTS] bit  NOT NULL,
    [IS_SURETY_BOND_REQ] bit  NOT NULL,
    [SPECIAL_INSURANCE_REQ] bit  NOT NULL,
    [SPECIAL_INSURANCE_REQ_OTHER] bit  NOT NULL,
    [DAMAGES_COVERAGE] nvarchar(25)  NOT NULL,
    [DAMAGES_RATE] decimal(10,5)  NOT NULL,
    [DAMAGES_AMT] decimal(20,2)  NOT NULL,
    [IS_AE_LICENSE_REQ] bit  NOT NULL,
    [AE_LICENSE_REQ_DATE] datetime  NOT NULL,
    [IS_CONTRACTOR_LICENSE_REQ] bit  NOT NULL,
    [CONTRACTOR_LICENSE_REQ_DATE] nvarchar(max)  NOT NULL,
    [IS_BUSINESS_LICENSE_REQ] bit  NOT NULL,
    [BUSINESS_LICENSE_REQ_DATE] datetime  NOT NULL,
    [GAD_IS_DAVIS_BACON] bit  NOT NULL,
    [GAD_IS_EXTERNAL_JOB] bit  NOT NULL,
    [GAD_IS_INTERNAL_JOB] bit  NOT NULL,
    [GAD_IS_ODP] bit  NOT NULL,
    [GAD_IS_PAR_ONSITE] bit  NOT NULL,
    [GAD_IS_VOUCHER_PACKAGE] bit  NOT NULL,
    [GAD_OTHER] nvarchar(500)  NOT NULL,
    [GAD_PERFORMANCE_OBLIGATIONS] nvarchar(200)  NOT NULL,
    [TAX_IS_RESEARCH_COMPLETE] bit  NOT NULL,
    [TAX_RESEARCH_COMPLETED_DATE] datetime  NOT NULL,
    [TAX_IS_EXCEMPT] bit  NOT NULL,
    [TAX_IS_EXCEMPT_EQUIPMENT_ONLY] bit  NOT NULL,
    [TAX_EXCEMPT_REGION] nvarchar(200)  NOT NULL,
    [TAX_EXCEMPT_EQUIPMENT_ONLY_REGION] nvarchar(200)  NOT NULL,
    [TAX_IS_STEEL_SHOP] bit  NOT NULL,
    [TAX_IS_PAID_BY_FOREIGN_ENTITY] bit  NOT NULL,
    [TAX_CURRENCY] nvarchar(200)  NOT NULL,
    [TAX_OTHER] nvarchar(500)  NOT NULL,
    [ENR_MARKET] nvarchar(200)  NOT NULL,
    [ENR_SUBMARKET] nvarchar(200)  NOT NULL,
    [PROSPECT_DATE_INITITATED] datetime  NOT NULL,
    [PROJECT_DATE_INITIATED] nvarchar(max)  NOT NULL,
    [DATE_CREATED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [DATE_APPROVAL_COMPLETE] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL,
    [DELIVERY_GROUP] nvarchar(200)  NOT NULL,
    [DIVISION] nvarchar(200)  NOT NULL
);
GO

-- Creating table 'SUBJOB'
CREATE TABLE [dbo].[SUBJOB] (
    [ERP_PROJECT_NO] nvarchar(12)  NOT NULL,
    [PCR_ID] bigint  NOT NULL,
    [PROJECT_TYPE] nvarchar(12)  NOT NULL,
    [DESCRIPTION] nvarchar(40)  NOT NULL,
    [DESIGN_MANAGER] nvarchar(100)  NOT NULL,
    [PROJECT_MANAGER] nvarchar(100)  NOT NULL,
    [DOC] nvarchar(100)  NOT NULL,
    [PROJECT_CONTROLLER] nvarchar(100)  NOT NULL,
    [ACCOUNTING_REP] nvarchar(100)  NOT NULL,
    [COST_CODE_TEMPLATE] nvarchar(100)  NOT NULL,
    [ERP_STATUS] nvarchar(50)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'DIVISION'
CREATE TABLE [dbo].[DIVISION] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [DELIVERY_GROUP] nvarchar(200)  NOT NULL,
    [DELIVERY_GRP_DESC] nvarchar(500)  NOT NULL,
    [DIVISION_DESC] nvarchar(200)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'EMPLOYEE'
CREATE TABLE [dbo].[EMPLOYEE] (
    [ID] bigint  NOT NULL,
    [NAME] nvarchar(200)  NOT NULL,
    [NETWORK_UID] nvarchar(12)  NOT NULL,
    [ACTIVE_FLAG] nvarchar(1)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'ENR_MARKET'
CREATE TABLE [dbo].[ENR_MARKET] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [MARKET] nvarchar(250)  NOT NULL,
    [SUBMARKET] nvarchar(250)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'NAICS'
CREATE TABLE [dbo].[NAICS] (
    [CODE] bigint  NOT NULL,
    [TITLE] nvarchar(200)  NOT NULL,
    [EFFECTIVE_YEAR] nvarchar(10)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'PROJECT_NAICS'
CREATE TABLE [dbo].[PROJECT_NAICS] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [PCR_ID] bigint  NOT NULL,
    [NAICS_CODE] bigint  NOT NULL,
    [NAICS_TITLE] nvarchar(200)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'SCOPE'
CREATE TABLE [dbo].[SCOPE] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(500)  NOT NULL,
    [CODE] nvarchar(25)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'PROJECT_SCOPE'
CREATE TABLE [dbo].[PROJECT_SCOPE] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [SCOPE_ID] bigint  NOT NULL,
    [PCR_ID] bigint  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'DISCIPLINE'
CREATE TABLE [dbo].[DISCIPLINE] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(500)  NOT NULL,
    [CODE] nvarchar(25)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'PROJECT_DISCIPLINE'
CREATE TABLE [dbo].[PROJECT_DISCIPLINE] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [DISCIPLINE_ID] bigint  NOT NULL,
    [PCR_ID] bigint  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'CONTRACT_TYPE'
CREATE TABLE [dbo].[CONTRACT_TYPE] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(500)  NOT NULL,
    [SERVICE_TYPE] nvarchar(150)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'GREEN_CERTS'
CREATE TABLE [dbo].[GREEN_CERTS] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(200)  NOT NULL,
    [CODE] nvarchar(12)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'CRM_CUSTOMER'
CREATE TABLE [dbo].[CRM_CUSTOMER] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [ERP_ID] nvarchar(12)  NOT NULL,
    [CRM_SYSTEM_ID] bigint  NOT NULL,
    [NAME] nvarchar(150)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'CRM_SYSTEM'
CREATE TABLE [dbo].[CRM_SYSTEM] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [NAME] nvarchar(150)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'SUBJOB_NAICS'
CREATE TABLE [dbo].[SUBJOB_NAICS] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [ERP_PROJECT_NO] nvarchar(12)  NOT NULL,
    [NAICS_CODE] bigint  NOT NULL,
    [NAICS_TITLE] nvarchar(200)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'SUBJOB_SCOPE'
CREATE TABLE [dbo].[SUBJOB_SCOPE] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [SCOPE_ID] bigint  NOT NULL,
    [ERP_PROJECT_NO] nvarchar(12)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'SUBJOB_DISCIPLINE'
CREATE TABLE [dbo].[SUBJOB_DISCIPLINE] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [DISCIPLINE_ID] bigint  NOT NULL,
    [ERP_PROJECT_NO] nvarchar(12)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'JOINT_VENTURE_PARTNER'
CREATE TABLE [dbo].[JOINT_VENTURE_PARTNER] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [PCR_ID] nvarchar(max)  NOT NULL,
    [NAME] nvarchar(150)  NOT NULL,
    [PERCENT_SPLIT] decimal(10,2)  NOT NULL,
    [DATE_ADDED] datetime  NOT NULL,
    [DATE_MODIFIED] datetime  NOT NULL,
    [CREATED_BY] nvarchar(50)  NOT NULL,
    [MODIFIED_BY] nvarchar(50)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [PCR_ID] in table 'PROJECT'
ALTER TABLE [dbo].[PROJECT]
ADD CONSTRAINT [PK_PROJECT]
    PRIMARY KEY CLUSTERED ([PCR_ID] ASC);
GO

-- Creating primary key on [ERP_PROJECT_NO] in table 'SUBJOB'
ALTER TABLE [dbo].[SUBJOB]
ADD CONSTRAINT [PK_SUBJOB]
    PRIMARY KEY CLUSTERED ([ERP_PROJECT_NO] ASC);
GO

-- Creating primary key on [ID] in table 'DIVISION'
ALTER TABLE [dbo].[DIVISION]
ADD CONSTRAINT [PK_DIVISION]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'EMPLOYEE'
ALTER TABLE [dbo].[EMPLOYEE]
ADD CONSTRAINT [PK_EMPLOYEE]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'ENR_MARKET'
ALTER TABLE [dbo].[ENR_MARKET]
ADD CONSTRAINT [PK_ENR_MARKET]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [CODE] in table 'NAICS'
ALTER TABLE [dbo].[NAICS]
ADD CONSTRAINT [PK_NAICS]
    PRIMARY KEY CLUSTERED ([CODE] ASC);
GO

-- Creating primary key on [ID] in table 'PROJECT_NAICS'
ALTER TABLE [dbo].[PROJECT_NAICS]
ADD CONSTRAINT [PK_PROJECT_NAICS]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'SCOPE'
ALTER TABLE [dbo].[SCOPE]
ADD CONSTRAINT [PK_SCOPE]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'PROJECT_SCOPE'
ALTER TABLE [dbo].[PROJECT_SCOPE]
ADD CONSTRAINT [PK_PROJECT_SCOPE]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'DISCIPLINE'
ALTER TABLE [dbo].[DISCIPLINE]
ADD CONSTRAINT [PK_DISCIPLINE]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'PROJECT_DISCIPLINE'
ALTER TABLE [dbo].[PROJECT_DISCIPLINE]
ADD CONSTRAINT [PK_PROJECT_DISCIPLINE]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'CONTRACT_TYPE'
ALTER TABLE [dbo].[CONTRACT_TYPE]
ADD CONSTRAINT [PK_CONTRACT_TYPE]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'GREEN_CERTS'
ALTER TABLE [dbo].[GREEN_CERTS]
ADD CONSTRAINT [PK_GREEN_CERTS]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'CRM_CUSTOMER'
ALTER TABLE [dbo].[CRM_CUSTOMER]
ADD CONSTRAINT [PK_CRM_CUSTOMER]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'CRM_SYSTEM'
ALTER TABLE [dbo].[CRM_SYSTEM]
ADD CONSTRAINT [PK_CRM_SYSTEM]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'SUBJOB_NAICS'
ALTER TABLE [dbo].[SUBJOB_NAICS]
ADD CONSTRAINT [PK_SUBJOB_NAICS]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'SUBJOB_SCOPE'
ALTER TABLE [dbo].[SUBJOB_SCOPE]
ADD CONSTRAINT [PK_SUBJOB_SCOPE]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'SUBJOB_DISCIPLINE'
ALTER TABLE [dbo].[SUBJOB_DISCIPLINE]
ADD CONSTRAINT [PK_SUBJOB_DISCIPLINE]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'JOINT_VENTURE_PARTNER'
ALTER TABLE [dbo].[JOINT_VENTURE_PARTNER]
ADD CONSTRAINT [PK_JOINT_VENTURE_PARTNER]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------