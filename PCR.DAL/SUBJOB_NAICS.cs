//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PCR.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class SUBJOB_NAICS
    {
        public long ID { get; set; }
        public string ERP_PROJECT_NO { get; set; }
        public long NAICS_CODE { get; set; }
        public string NAICS_TITLE { get; set; }
        public System.DateTime DATE_ADDED { get; set; }
        public System.DateTime DATE_MODIFIED { get; set; }
        public string CREATED_BY { get; set; }
        public string MODIFIED_BY { get; set; }
    }
}
