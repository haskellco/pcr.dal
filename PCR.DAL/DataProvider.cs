﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PCR.DAL
{
    public class DataProvider
    {
        
        public List<JOINT_VENTURE_PARTNER> GetJVPartners(long id)
        {
            List<JOINT_VENTURE_PARTNER> result = new List<JOINT_VENTURE_PARTNER>();

            using (PCREntities context = new PCREntities())
            {
                result = context.JOINT_VENTURE_PARTNER.Where(jv => jv.PCR_ID==id).ToList<JOINT_VENTURE_PARTNER>();
            }

            return result;
        }

        public List<SCOPE> GetScopesByCategory(string category = "")
        {
            List<SCOPE> scopes = new List<SCOPE>();

            using (PCREntities context = new PCREntities())
            {
                scopes =  context.SCOPEs.Where(s => s.CATEGORY.ToLower() == category.ToLower()).ToList<SCOPE>();
            }

            return scopes;
        }

        public PROJECT GetPCRRecord(long pcrId)

        {
            PROJECT project = new PROJECT();

            using (PCREntities context = new PCREntities())
            {
                project =  context.PROJECTs.FirstOrDefault(p => p.PCR_ID == pcrId);
                var div = context.DIVISIONs.FirstOrDefault(d => d.DIVISION_CODE == project.DIVISION || d.DIVISION_DESC == project.DIVISION);
                project.DIVISION = (div != null) ? div.DIVISION_DESC : "";
                project.DELIVERY_GROUP = (div != null) ? div.DELIVERY_GRP_DESC : "";
                var deliveryMethod = context.DELIVERY_METHOD.FirstOrDefault(dm => dm.CODE == project.DELIVERY_METHOD);
                project.DELIVERY_METHOD = (deliveryMethod == null) ? "" : deliveryMethod.DESCRIPTION;
                project.GONOGO_ANALYSIS_DOC_PATH = WebUtility.HtmlEncode(project.GONOGO_ANALYSIS_DOC_PATH);
                project.RISK_ANALYSIS_DOC_PATH = WebUtility.HtmlEncode(project.RISK_ANALYSIS_DOC_PATH);
            }

            return project;
        }

        public List<SCOPE> GetPotentialScopes(long pcrId)
        {
            List<SCOPE> result = new List<SCOPE>();

            using (PCREntities context = new PCREntities())
            {
                var projectScopes = context.PROJECT_SCOPE.Where(p => p.PCR_ID == pcrId).ToList<PROJECT_SCOPE>().Select(ps => ps.SCOPE_ID);
                var scopes = GetScopesByCategory("HASKELL");
                result = scopes.Where(s => projectScopes.Contains(s.ID)).ToList();
            }

            return result;
        }
        // Added on 09/03/2020 for AE Entity requirement
        public List<ENTITY> GetHaskellEntities()
        {
            List<ENTITY> result = new List<ENTITY>();

            using (PCREntities context = new PCREntities())
            {
                var es = context.ENTITies.ToList<ENTITY>();
                result = es;
            }

            return result;
        }

        public List<SCOPE> GetSF330Scopes(long pcrId)
        {
            List<SCOPE> result = new List<SCOPE>();

            using (PCREntities context = new PCREntities())
            {
                var projectScopes = context.PROJECT_SCOPE.Where(p => p.PCR_ID == pcrId).ToList<PROJECT_SCOPE>().Select(ps => ps.SCOPE_ID);
                var scopes = GetScopesByCategory("SF330");
                result = scopes.Where(s => projectScopes.Contains(s.ID)).ToList();
            }

            return result;
        }

        public List<DISCIPLINE> GetProjectDisciplines(long pcrId)
        {
            List<DISCIPLINE> result = new List<DISCIPLINE>();

            using (PCREntities context = new PCREntities())
            {
                var projectDiscipline = context.PROJECT_DISCIPLINE.Where(p => p.PCR_ID == pcrId).ToList<PROJECT_DISCIPLINE>().Select(ps => ps.DISCIPLINE_ID);                
                result = context.DISCIPLINEs.Where(s => projectDiscipline.Contains(s.ID)).ToList();
            }

            return result;
        }

        public List<OPERATING_UNIT> GetOperatingUnits()
        {
            List<OPERATING_UNIT> result = new List<OPERATING_UNIT>();

            using (PCREntities context = new PCREntities())
            {
                result = context.OPERATING_UNIT.ToList<OPERATING_UNIT>();
            }

            return result;
        }

        public List<PROJECT_NAICS> GetProjectNAICS(long pcrId)
        {
            List<PROJECT_NAICS> result = new List<PROJECT_NAICS>();

            using (PCREntities context = new PCREntities())
            {
                result = context.PROJECT_NAICS.Where(p => p.PCR_ID == pcrId).ToList<PROJECT_NAICS>();
                
            }

            return result;
        }

        public List<SUBJOB> GetSubJobs(long pcrId)
        {
            List<SUBJOB> sj = new List<SUBJOB>();

            using (PCREntities context = new PCREntities())
            {
                sj = context.SUBJOBs.Where(s => s.PCR_ID == pcrId).ToList<SUBJOB>();
            }

            return sj;
        }

        public List<vw_ProspectApprovers> GetProspectApprovers(long pcrId)
        {
            List<vw_ProspectApprovers> result = new List<vw_ProspectApprovers>();

            using (PCREntities context = new PCREntities())
            {
                result = context.vw_ProspectApprovers.Where(app => app.PCR_ID == pcrId).ToList();
            }

            return result;
        }

        public List<vw_ProjectApprovers> GetProjectApprovers(long pcrId)
        {
            List<vw_ProjectApprovers> result = new List<vw_ProjectApprovers>();

            using (PCREntities context = new PCREntities())
            {
                result = context.vw_ProjectApprovers.Where(app => app.PCR_ID == pcrId).ToList();
            }

            return result;
        }

        public List<sp_GetRequiredApprovals_Result> GetRequiredApprovals(long pcrID, string projectStage)
        {
            using (PCREntities context = new PCREntities())
            {
                var result = context.sp_GetRequiredApprovals(pcrID, projectStage).ToList<sp_GetRequiredApprovals_Result>();
                return result;
            }
          
        }

        public RELATED_PROJECT_SCENARIO GetRelatedProjectScenario(long scenarioID)
        {
            using (PCREntities context = new PCREntities())
            {
                try
                {
                    var result = context.RELATED_PROJECT_SCENARIO.FirstOrDefault(ps => ps.ID == scenarioID);
                    return result;
                }
                catch (Exception e)
                {

                    throw e;
                }                
            }
        }
    }
}
