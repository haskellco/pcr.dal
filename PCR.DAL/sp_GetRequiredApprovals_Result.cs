//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PCR.DAL
{
    using System;
    
    public partial class sp_GetRequiredApprovals_Result
    {
        public Nullable<int> Step { get; set; }
        public string Approvalname { get; set; }
        public string Approval { get; set; }
        public string Approvers { get; set; }
        public string ApproverNames { get; set; }
        public string ResponsesRequired { get; set; }
        public string ApprovalResponse { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovalDate { get; set; }
        public string SubmissionStatus { get; set; }
        public Nullable<int> CountOfCriteriaMet { get; set; }
    }
}
