﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PCR.DAL;

namespace PCR.DAL.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestPotentialScopes(int id)
        {
            id = 1;
            DataProvider dataProvider = new DataProvider();

            var prospect = dataProvider.GetPCRRecord(id);
            var scopes = dataProvider.GetPotentialScopes(id);
            var subjobs = dataProvider.GetSubJobs(id);
        }
    }
}
